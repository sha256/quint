__author__ = 'sha256'
from apps.securedl.models import SecuredFile, FileSession
from datetime import datetime
import hashlib


def add_session(model, validity, attrb=None):

    if not isinstance(model, SecuredFile):
        if not attrb:
            raise TypeError("attrb shouldn't be None")
        try:
            model = getattr(model, attrb)
        except AttributeError, e:
            raise e

    session = FileSession()
    hashkey = hashlib.sha256("%s%s%s" % (model.name, model.last_access, datetime.now())).hexdigest()
    session.hashkey = hashkey
    session.sfile = model
    session.validity = validity
    session.save()

    return hashkey
