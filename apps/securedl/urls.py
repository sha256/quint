__author__ = 'sha256'

from django.conf.urls import url, patterns
from apps.securedl.views import download

urlpatterns = patterns('',
    url(r'^download/(?P<session_hash>(.*))', download, name='secure-download'),
)