__author__ = 'sha256'

from django.db import models
from datetime import datetime
from django.core.files.storage import FileSystemStorage
import os

file_storage = FileSystemStorage(location="G:/Projects/Django/quint/static")


class SecuredFile(models.Model):

    name = models.CharField(max_length=255)
    file_path = models.FileField(storage=file_storage, upload_to="yellow")
    hits = models.IntegerField(default=-1)
    created = models.DateTimeField(blank=True, null=True)
    last_access = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.created:
            self.created = datetime.now()
            self.last_access = datetime.now()

        self.last_access = datetime.now()
        self.hits += 1
        super(SecuredFile, self).save(*args, **kwargs)


class FileSession(models.Model):

    sfile = models.ForeignKey(SecuredFile)
    validity = models.IntegerField()
    hashkey = models.CharField(max_length=64)
    created = models.DateTimeField(null=True, blank=True)
    hits = models.IntegerField(default=-1)
    last_access = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.created:
            self.created = datetime.now()
            self.last_access = datetime.now()

        self.hits += 1
        self.last_access = datetime.now()
        super(FileSession, self).save(*args, **kwargs)


class MyApps(models.Model):

    name = models.CharField(max_length=255)
    secure = models.ForeignKey(SecuredFile, null=True, blank=True)
    file_path = models.FileField(storage=file_storage, upload_to="yellow")
    hits = models.IntegerField(default=-1)

    def save(self, *args, **kwargs):
        self.hits += 1
        super(MyApps, self).save(*args, **kwargs)


