__author__ = 'sha256'

from django.core.servers.basehttp import FileWrapper
from django.http import HttpResponse, Http404
from apps.securedl.models import FileSession
import os
from datetime import datetime, timedelta


def download(request, session_hash, resume_support=True, *args, **kwargs):
    session = None
    try:
        session = FileSession.objects.get(hashkey=session_hash)
        session.save() # update hits, last_access
    except FileSession.DoesNotExist:
        raise Http404("File Not Found")

    if datetime.now() - timedelta(hours=session.validity) >= session.created:
        return HttpResponse("File Expired")

    file_path = session.sfile.file_path.path

    resumable = True
    size = os.path.getsize(file_path)
    s_start = 0
    s_end = size - 1
    resp_status = 200
    
    try:
        ss, se = request.META['HTTP_RANGE'].split('=')[1].split('-')
        if resume_support:
            resp_status = 206
            if ss.strip():
                s_start = int(ss.strip())
            if se.strip():
                s_end = int(se.strip())
    except KeyError:
        resumable = False

    # s_end should be less than size
    s_end = min(s_end, size - 1)
    # if s_start negative make it 0
    s_start = max(abs(s_start), 0)
    if s_start > size:
        # this shouldn't happen, but ...
        pass

    try:
        f = open(file_path, "rb")
        f.seek(s_start)
    except:
        # log error
        pass

    response = HttpResponse(FileWrapper(f), content_type="application/octet-stream", status=resp_status)
    #response = HttpResponse(FileWrapper(file(file_path, "rb")), content_type="application/octet-stream") #faster
    response['Content-Disposition'] = "attachement; filename=%s" % session.sfile.name
    response['Accept-Ranges'] = "bytes"
    if resumable and resume_support:
        response['Content-Range'] = "bytes %s-%s/%s" % (s_start, s_end, size)
    response['Content-Length'] = s_end - s_start + 1
    
    return response
