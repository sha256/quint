__author__ = 'sha256'
from django.db import models
from apps.auth.models import User
from core.models import DomainEntity


class Token(DomainEntity):

    user = models.ForeignKey(User)
    token = models.CharField(max_length=200)