from apps.auth.models import User
from apps.auth.authentication import authenticate
from django.core import serializers
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from apps.tokenauth.decorators import token_required
from core.views import SmartView

from .tokens import token_generator
import json


def JSONResponse(data):
    try:
        data['errors']
    except KeyError:
        data['success'] = True
    except TypeError:
        pass

    jsonx = json.JSONEncoder(indent=4)
    ret = jsonx.encode(data)
    return HttpResponse(ret, mimetype="application/json")


def JSONError(error_string):
    data = {
        'success': False,
        'errors': error_string,
    }
    jsonx = json.JSONEncoder(indent=4)
    ret = jsonx.encode(data)
    return HttpResponse(ret, mimetype="application/json")


@csrf_exempt
def token_new(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                TOKEN_CHECK_ACTIVE_USER = getattr(settings, "TOKEN_CHECK_ACTIVE_USER", False)

                if TOKEN_CHECK_ACTIVE_USER and not user.is_active:
                    return JSONError("User account is disabled.")

                data = {
                    'token': token_generator.create_new(user).token,
                    'user': {
                        'id': user.id,
                        'first_name': user.first_name,
                        'last_name': user.last_name,
                        'email': user.email,
                        'country_code': user.country.code,
                        'birth_date': user.birth_date.__str__(),
                        'sex': user.sex,
                        'image_url': user.propic.url if user.propic else None
                    }
                }
                return JSONResponse(data)
            else:
                return JSONError("Unable to log you in, please try again.")
        else:
            return JSONError("Must include 'username' and 'password' as POST parameters.")
    else:
        return JSONError("Must access via a POST request.")


def token(request, token, user):
    try:
        user = User.objects.get(pk=user)
    except User.DoesNotExist:
        return JSONError("User does not exist.")

    TOKEN_CHECK_ACTIVE_USER = getattr(settings, "TOKEN_CHECK_ACTIVE_USER", False)

    if TOKEN_CHECK_ACTIVE_USER and not user.is_active:
        return JSONError("User account is disabled.")

    if token_generator.check_token(user.id, token):
        return JSONResponse({})
    else:
        return JSONError("Token did not match user.")


class TokeAuthMixin(object):

    @method_decorator(token_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TokeAuthMixin, self).dispatch(request, *args, **kwargs)


class TokenView(TokeAuthMixin, SmartView):
    pass


class TokenViewWithNoCsrf(SmartView):

    @method_decorator(token_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(TokenViewWithNoCsrf, self).dispatch(request, *args, **kwargs)