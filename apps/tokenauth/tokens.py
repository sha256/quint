"""django.contrib.auth.tokens, but without using last_login in hash"""

from datetime import date
from django.conf import settings
import string
from django.utils.http import int_to_base36, base36_to_int
from django.utils.crypto import get_random_string
from .models import Token


VALID_KEY_CHARS = string.ascii_lowercase + string.digits


class TokenGenerator(object):

    TOKEN_TIMEOUT_DAYS = getattr(settings, "TOKEN_TIMEOUT_DAYS", 7)

    def get_token(self, user):
        try:
            token = Token.objects.get(user__id=user)
        except Token.DoesNotExist:
            return None
        return token

    def check_token(self, user, token):
        tob = self.get_token(user)
        if tob is not None and tob.token == token:
            return True
        return False

    def create_new(self, user_ob):
        Token.objects.filter(user=user_ob).delete()
        tok = Token.objects.create(user=user_ob, token=self._get_new_token())
        return tok

    def _get_new_token(self):
        while True:
            token_key = get_random_string(32, VALID_KEY_CHARS)
            try:
                Token.objects.get(token=token_key)
            except Token.DoesNotExist:
                return token_key

token_generator = TokenGenerator()
