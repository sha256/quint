from django.http import HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

from functools import wraps
from apps.auth.models import User
from .tokens import token_generator
from apps.auth.authentication import login


def token_required(view_func):
    @csrf_exempt
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        user = None
        token = None
        basic_auth = request.META.get('HTTP_AUTHORIZATION')

        if basic_auth:
            auth_method, auth_string = basic_auth.split(' ', 1)

            if auth_method.lower() == 'basic':
                #auth_string = auth_string.strip().decode('base64')
                user, token = auth_string.split(':', 1)

        if not (user and token):
            user = request.REQUEST.get('user')
            token = request.REQUEST.get('token')

        if user and token:
            if token_generator.check_token(user, token):
                user_ob = User.objects.get(pk=user)
                login(request, user_ob)
                return view_func(request, *args, **kwargs)
        return HttpResponseForbidden()
    return _wrapped_view
