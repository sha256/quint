from django.conf.urls import patterns, url


urlpatterns = patterns('apps.tokenauth.views',
    url(r'^token/new$', 'token_new', name='api_token_new'),
    url(r'^token/(?P<user>\d+)/(?P<token>.*)$', 'token', name='api_token'),
)
