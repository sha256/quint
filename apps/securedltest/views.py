__author__ = 'sha256'


from core.views import SmartView
from django import forms
from apps.securedl.models import SecuredFile, MyApps
from apps.securedl.session import add_session


class SDForm(forms.ModelForm):

    class Meta:
        model = MyApps
        exclude = ('secure', )


class SDtest(SmartView):

    def get(self, request, *args, **kwargs):

        #f = SecuredFile.objects.get(pk=5)
        m = MyApps.objects.get(pk=1)
        # y = SecuredFile()
        # y.name = m.name
        # y.file_path = m.file_path
        # y.save()
        # m.secure = y
        # m.save()
        url = add_session(m, 22, 'secure')


        return self.render(request, "test/securedltest.html", {'form': SDForm(), 'file': url})

    def post(self, request, *args, **kwargs):

        form = SDForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            form = SDForm()

        return self.render(request, "test/securedltest.html", {'form': form})



