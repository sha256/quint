__author__ = 'sha256'

from django.utils.importlib import import_module
from django.utils.module_loading import module_has_submodule
import inspect
import pkgutil


def load_app(app_name):

    #app_module = import_module(app_name)

    try:
        views = import_module('.views', app_name)
        print inspect.ismodule(views)

    except ImportError:
        pass
        # if not module_has_submodule(app_module, 'models'):
        #     return None
        # else:
        #     return False

    #for name, obj in inspect.getmembers(views)

    return inspect.getmembers(views, inspect.ismodule)

