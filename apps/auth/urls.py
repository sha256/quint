from apps.auth.views.pwd_reset import PasswordResetView

__author__ = 'sha256'

from django.conf.urls import patterns, url
from apps.auth.views.login import LoginView, LogoutView
from apps.auth.views.roles import RolesView
from apps.auth.views.ajax import DataTableAjax
from apps.auth.views.user import UserView, UserEditView, MyAccountView

urlpatterns = patterns('',
    url(r'login/$', LoginView.as_view(), name='auth-login'),
    url(r'logout/$', LogoutView.as_view(), name="auth-logout"),
    url(r'roles/$', RolesView.as_view(), name="auth-roles"),
    url(r'users/$', UserView.as_view(), name="auth-users"),
    url(r'users/edit/(?P<user_id>(\d+))$', UserEditView.as_view(), name="auth-users-edit"),
    url(r'^myaccount/$', MyAccountView.as_view(), name="auth-myaccount"),
    url(r'^passwordreset/(?P<code>(.+))$', PasswordResetView.as_view()),
    #url(r'^ajax/$', DataTableAjax.as_view()),

)
