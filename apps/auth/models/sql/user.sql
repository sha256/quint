INSERT INTO auth_user (password, is_superuser, username, email, is_active, last_login, date_joined, first_name, last_name)
    VALUES ("pbkdf2_sha256$10000$4ePkUE17mn5h$5CUHtlU02fMHl9ljCOdwx++eGO11qHs2yHDNusWPq3Y=",
    1,
    "admin",
    "foo@foo.com",
    1,
    "0000-00-00 00:00:00",
    "0000-00-00 00:00:00",
    "Shamim",
    "Hasnath"
);