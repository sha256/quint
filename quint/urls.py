from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from apps.test import TestView
from apps.auth.views import LoginView
from quint.settings import STATIC_ROOT, STATIC_URL, MEDIA_ROOT, MEDIA_URL, DEBUG
from django.views.static import serve
from apps.securedltest.views import SDtest
from core.utils.misc import autourlpatters

from apps.controller.loader import load_app

#print load_app('apps.controller')
#print load_app('apps.auth')

urlpatterns = patterns('',
    url(r'^', include('apps.auth.urls')),
    url(r'^test/$', TestView.as_view()),
    url(r'^secure/', include('apps.securedl.urls')),
    url(r'^securedltest/$', SDtest.as_view()),
)

urlpatterns += autourlpatters()

# url pattern for static files
urlpatterns += static(STATIC_URL, document_root=STATIC_ROOT, show_indexes=DEBUG)

# url for uploaded media files
urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT, show_indexes=DEBUG)
